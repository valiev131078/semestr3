import socket
import random
import threading


def main():
    host = 'localhost'
    port = 8003
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((host, port))
        read_file = sock.makefile(mode="r", encoding="utf-8")
        write_file = sock.makefile(mode="w", encoding="utf-8")
        while True:
            data = read_file.readline().strip()
            while data:
                print(data)
                data = read_file.readline().strip()
            a = input("send: ")
            if a == "exit":
                break
            write_file.write(a + "\n")
        write_file.flush()
    sock.close()


if __name__ == "__main__":
    main()
