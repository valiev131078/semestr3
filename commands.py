import functools
import os.path

import dictionaries


def register_command(func):
    dictionaries.commands_dict[func.__name__] = func
    return func


@register_command
def arrow(addr):
    for i in dictionaries.choice_game[addr]:
        if i == 'arrow':
            #посылает этот i серверу(потом удаляет)
            dictionaries.choice_game.remove(i)
            with open("txt/arrow.txt") as f:
                for i in f:
                    return print(i)

@register_command
def helmet(addr):
    for i in dictionaries.choice_game[addr]:
        if i == 'helmet':
            #посылает этот i серверу(потом удаляет)
            dictionaries.choice_game.remove(i)
            with open("txt/helmet.txt") as f:
                for i in f:
                    return print(i)


@register_command
def sword(addr):
    for i in dictionaries.choice_game[addr]:
        if i == 'sword':
            #посылает этот i серверу(потом удаляет)
            dictionaries.choice_game.remove(i)
            with open("txt/sword.txt") as f:
                for i in f:
                    return print(i)

@register_command
def shield(addr):
    for i in dictionaries.choice_game[addr]:
        if i == 'shield':
            #посылает этот i серверу(потом удаляет)
            dictionaries.choice_game.remove(i)
            with open("txt/shield.txt") as f:
                for i in f:
                    return print(i)

@register_command
def point(addr):
    if data == "Нет отбивки":
        point[addr] += 1

    return  "+ 1 point"