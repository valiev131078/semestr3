import os
import socket
import threading
from concurrent.futures import ThreadPoolExecutor
import random
import dictionaries
# No inspection is added because these imports register command functions
# noinspection PyUnresolvedReferences
from commands import *


def handle_command(addr, cmd, *args):
    try:
        if not addr==dictionaries.current_step:
            return "Не ваш ход"
        command_func = dictionaries.commands_dict.get(cmd)
        if command_func is None:
            raise ValueError('Unknown command')
        result = command_func(addr, *args)
        dictionaries.current_step = (dictionaries.step.index(dictionaries.current_step) + 1) % (len(dictionaries.step) - 1)
    except (TypeError, ValueError, AssertionError) as e:
        result = str(e)
    except StopIteration:
        result = ''
    return result


def handle_client(conn, addr):
    read_file = conn.makefile(mode='r', encoding='utf-8')
    write_file = conn.makefile(mode='w', encoding='utf-8')
    write_file.write('Hi client\n')

    if addr not in dictionaries.step:
        if dictionaries.current_step == "":
            dictionaries.current_step = addr
        dictionaries.step.append(addr)
        for i in range(6):
            dictionaries.choice_game[addr] = random.choice(dictionaries.tools)
        #не выводить список разобраться!
            write_file.write("".join(dictionaries.choice_game[addr]) + '\n')
            write_file.flush()
    print(f'Using thread {threading.get_ident()} for client: {addr}')
    write_file.flush()
    cmd = read_file.readline().strip()
    while cmd:
        result = handle_command(addr, *cmd.split())
        if result == '':
            break
        write_file.write(result + '\n')
        write_file.flush()
        cmd = read_file.readline().strip()
    conn.close()


def main():
    host = 'localhost'
    port = 8003
    print(f'Started process with PID={os.getpid()}')

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # solution for "[Error 89] Address already in use". Use before bind()
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(2)
        #  Proper threads termination doesn't work here, use SIGKILL
        with ThreadPoolExecutor(max_workers=2) as executor:
            while True:
                conn, addr = s.accept()
                executor.submit(handle_client, conn, addr)


if __name__ == '__main__':
    main()
